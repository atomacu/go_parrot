import { BookInterface } from "./interfaces";
const books: Array<BookInterface> = [
  {
    id: "4561514",
    cover_img: "https://covers.openlibrary.org/b/id/1471849-L.jpg",
    title: "Ruby",
    author: "Lauraine Snelling",
    description:
      " In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.",
    category: "Romance",
    pages: 559,
  },
  {
    id: "1590352",
    cover_img: "https://covers.openlibrary.org/b/id/9279052-L.jpg",
    title: "Jane Austen",
    author: "Jane Austen, John Halperin",
    description:
      "Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum.",
    category: "Romance",
    pages: 334,
  },
  {
    id: "131113",
    cover_img: "https://archive.org/services/img/darktowergunslin00king",
    title: "The Dark tower: the gunslinger",
    author: "King, Stephen, 1947-",
    description:
      "On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy.",
    category: "Thrillers",
    pages: 456,
  },
  {
    id: "3610405",
    title: "An essay on typography",
    author: "Eric Gill",
    description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    cover_img: "https://covers.openlibrary.org/b/id/4733376-L.jpg",
    category: "Arts",
    pages: 133,
  },
  {
    id: "4836254",
    cover_img: "https://covers.openlibrary.org/b/id/5430295-L.jpg",
    title: "Structure of the visual book",
    author: "Keith A. Smith",
    description:
      " Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)",
    category: "Arts",
    pages: 113,
  },
  {
    id: "4038582",
    cover_img: "https://covers.openlibrary.org/b/id/6383286-L.jpg",
    title: "Biology",
    author: "Cecie Starr, Ralph Taggart, Starr",
    description:
      "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.",
    category: "Science & Mathematics",
    pages: 776,
  },
  {
    id: "576357",
    cover_img:
      "https://ia800609.us.archive.org/view_archive.php?archive=/21/items/olcovers682/olcovers682-L.zip&file=6820464-L.jpg&ext=",
    title: "The wisdom of the body",
    author: "Sherwin B. Nuland",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
    category: "Science & Mathematics",
    pages: 395,
  },
  {
    id: "560656",
    cover_img: "https://covers.openlibrary.org/b/id/6623390-L.jpg",
    title: "The fortune of war",
    author: "Patrick O'Brian",
    description:
      "But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter ",
    category: "Places",
    pages: 329,
  },
  {
    id: "5317432",
    cover_img: "https://covers.openlibrary.org/b/id/3819253-L.jpg",
    title: "Serving children with disabilities",
    author: "Laudan Y. Aron",
    description:
      "Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat",
    category: "Child Health Services",
    pages: 182,
  },
  {
    id: "7341508",
    cover_img: "https://covers.openlibrary.org/b/id/9441342-L.jpg",
    title: "Psychology",
    author: "Charles G. Morris, Albert A. Maisto",
    description:
      "On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their",
    category: "Textbooks",
    pages: 597,
  },
];

export default books;
