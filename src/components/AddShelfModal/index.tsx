import React, { useState, SyntheticEvent, useEffect } from "react";
import { Button, Modal, Row, Col } from "react-bootstrap";
import { ShelfInterface } from "../../interfaces";

export default function AddShelfModal({
  categories,
  setShelves,
}: {
  categories: Array<string>;
  setShelves: Function;
}) {
  const [showAddNewShelf, setShowAddNewShelf] = useState(false);
  const handleCloseAddNewShelfModal = () => setShowAddNewShelf(false);
  const handleShowAddNewShelfModal = () => setShowAddNewShelf(true);

  const [shelfName, setShelfName] = useState("");
  useEffect(() => {
    setShelves(JSON.parse(localStorage.getItem("shelves")));
  }, [shelfName]);

  const [shelfCategory, setShelfCategory] = useState("");
  const addShelfToLocal = (e: SyntheticEvent) => {
    e.preventDefault();
    let shelves: Array<ShelfInterface> = [];
    if (localStorage.getItem("shelves"))
      shelves = JSON.parse(localStorage.getItem("shelves"));

    shelves = [
      ...shelves,
      {
        name: shelfName,
        category: shelfCategory,
        review: "",
        rating: -1,
        booksIds: [],
      },
    ];
    localStorage.setItem("shelves", JSON.stringify(shelves));
    setShelfCategory("");
    setShelfName("");
    handleCloseAddNewShelfModal();
  };

  const handleShelfName = (e: SyntheticEvent) => {
    // @ts-ignore
    setShelfName(e.currentTarget.value);
  };

  const handleShelfCategory = (e: SyntheticEvent) => {
    // @ts-ignore
    setShelfCategory(e.currentTarget.value);
    // @ts-ignore
    console.log(e.currentTarget.value);
  };
  return (
    <>
      <Button
        block
        className="font-weight-bold rounded-0"
        onClick={handleShowAddNewShelfModal}
      >
        + Add new Shelf
      </Button>
      <Modal show={showAddNewShelf} onHide={handleCloseAddNewShelfModal}>
        <Modal.Header closeButton>
          <Modal.Title>Add new Shelf</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form onSubmit={addShelfToLocal}>
            <Row className="justify-content-center mb-2">
              <Col md={10}>
                <input
                  onChange={handleShelfName}
                  value={shelfName}
                  type="text"
                  placeholder="Shelf name"
                  className="form-control rounded-0"
                  required
                />
              </Col>
            </Row>
            <Row className="justify-content-center">
              <Col md={10}>
                <select
                  value={shelfCategory}
                  onChange={handleShelfCategory}
                  className="form-control rounded-0"
                  required
                >
                  <option value="" disabled>
                    Select category
                  </option>
                  <option value="none">None</option>
                  {categories.map((category: string, index: number) => (
                    <option value={category} key={index}>
                      {category}
                    </option>
                  ))}
                </select>
              </Col>
            </Row>
            <Row className="justify-content-center mt-2">
              <Col md={10}>
                <Button
                  block
                  type="submit"
                  className="rounded-0 font-weight-bold"
                >
                  Add
                </Button>
              </Col>
            </Row>
          </form>
        </Modal.Body>
      </Modal>
    </>
  );
}
