import React, { useState, SyntheticEvent } from "react";
import TextRed from "../../styledComponents/TextRed";
import { BookInterface } from "../../interfaces";
import { Button, Modal, Alert } from "react-bootstrap";

export default function AddToShelveButton({
  currentBook,
}: {
  currentBook: BookInterface;
}) {
  const shelves = JSON.parse(localStorage.getItem("shelves"));
  const [selectedShelfName, setSelectedShelfName] = useState("");
  const [allreadyIncludes, setAllreadyIncludes] = useState(false);
  const [addedBookToShelfAlert, setAddedBookToShelfAlert] = useState(false);
  const [showAddToShelf, setShowAddToShelf] = useState(false);
  const handleShowAddToShelfModal = () => setShowAddToShelf(true);
  const handleCloseAddToShelfModal = () => setShowAddToShelf(false);

  const addBookToShelf = () => {
    let localShelves = [...shelves];

    localShelves = localShelves.reduce((acc, item, index) => {
      if (item.booksIds.includes(currentBook.id)) {
        setAllreadyIncludes(true);
      } else if (item.name === selectedShelfName) {
        item.booksIds = [...localShelves[index].booksIds, currentBook.id];
        setAddedBookToShelfAlert(true);
        setTimeout(() => {
          handleCloseAddToShelfModal();
          setAddedBookToShelfAlert(false);
        }, 2000);
      }
      return [...acc, item];
    }, []);

    localStorage.setItem("shelves", JSON.stringify(localShelves));
  };

  const handleSelectedShelfName = (e: SyntheticEvent) => {
    //@ts-ignore
    setSelectedShelfName(e.currentTarget.value);

    setAllreadyIncludes(false);
  };
  return (
    <>
      <Button
        block
        onClick={handleShowAddToShelfModal}
        className="rounded-0 mt-3"
      >
        Add to Shelf
      </Button>

      <Modal
        show={showAddToShelf}
        className="rounded-0"
        onHide={handleCloseAddToShelfModal}
        centered
      >
        {!addedBookToShelfAlert ? (
          <>
            <Modal.Header>
              <Modal.Title className="text-center w-100">
                Add book to shelf
              </Modal.Title>
            </Modal.Header>
            <Modal.Body className="rounded-0">
              {!!shelves.filter(
                (item: any) =>
                  item.category === currentBook.category ||
                  item.category === "none"
              ).length ? (
                <>
                  <select
                    value={selectedShelfName}
                    className="form-control rounded-0"
                    onChange={handleSelectedShelfName}
                  >
                    <option value="">Select shelf</option>
                    {shelves &&
                      shelves
                        .filter(
                          (item: any) =>
                            item.category === currentBook.category ||
                            item.category === "none"
                        )
                        .map((item: any, index: number) => (
                          <option value={item.name} key={index}>
                            {`Shelf: ${item.name} | Category: ${item.category}`}
                          </option>
                        ))}
                  </select>
                  <Button
                    onClick={addBookToShelf}
                    className="mt-2 rounded-0"
                    block
                  >
                    Add
                  </Button>
                  {allreadyIncludes && (
                    <TextRed className="text-center mb-0 mt-2">
                      This book is already in this shelf
                    </TextRed>
                  )}
                </>
              ) : (
                <a href="/shelves">
                  Shelf with this category don't exist. Click here to create one
                </a>
              )}
            </Modal.Body>
          </>
        ) : (
          <Modal.Body className="p-0">
            <Alert variant="success" className="text-center mb-0">
              Book added ! ! !
            </Alert>
          </Modal.Body>
        )}
      </Modal>
    </>
  );
}
