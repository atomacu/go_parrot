import React, { useState, useContext } from "react";
import Img from "../../styledComponents/Img";
import { BookInterface } from "../../interfaces";
import Pointer from "../../styledComponents/Pointer";
import AddToShelfButton from "../AddToShelfButton";
import { Card, Modal, Col, Row } from "react-bootstrap";
import { ThemeContext } from "../../contexts";

export default function Book({
  book,
  addToShelfButton,
}: {
  book: BookInterface;
  addToShelfButton: boolean;
}) {
  const [showBook, setShowBook] = useState(false);
  const handleCloseBookModal = () => setShowBook(false);
  const handleShowBookModal = () => setShowBook(true);

  const [currentBook, setCurrentBook] = useState({
    id: "",
    title: "",
    author: "",
    description: "",
    cover_img: "",
    category: "",
    pages: 0,
  });

  const handleBookClick = () => {
    handleShowBookModal();
    setCurrentBook(book);
  };
  const { themeVariant, themeTextVariant } = useContext(ThemeContext);
  return (
    <>
      <Pointer className="h-100">
        <Card className="rounded-0 h-100" onClick={handleBookClick}>
          <Card.Img className="rounded-0" variant="top" src={book.cover_img} />
          <Card.Body
            className={"p-2 bg-" + themeVariant + " text-" + themeTextVariant}
          >
            <Card.Title className="text-center">{book.title}</Card.Title>
            <Card.Text className="text-truncate">
              <span className="font-weight-bold">Category: </span>
              {book.category}
            </Card.Text>
          </Card.Body>
        </Card>
      </Pointer>
      <Modal
        size="lg"
        show={showBook}
        className="rounded-0"
        onHide={handleCloseBookModal}
      >
        <Modal.Body
          className={
            "rounded-0 bg-" + themeVariant + " text-" + themeTextVariant
          }
        >
          <Row>
            <Col>
              <Img src={currentBook.cover_img} />
            </Col>
            <Col>
              <h4 className="text-center">{currentBook.title}</h4>
              <p>{currentBook.description}</p>
              <p className="mb-0">
                <span className="font-weight-bold">Author: </span>
                {currentBook.author}
              </p>
              <p className="mb-0">
                <span className="font-weight-bold">Category: </span>
                {currentBook.category}
              </p>
              <p className="mb-0">
                <span className="font-weight-bold">Pages: </span>
                {currentBook.pages}
              </p>
              {addToShelfButton && (
                <AddToShelfButton currentBook={currentBook} />
              )}
            </Col>
          </Row>
        </Modal.Body>
      </Modal>
    </>
  );
}
