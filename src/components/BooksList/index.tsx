import React from "react";
import Book from "../Book";
import { Row, Col } from "react-bootstrap";
import { BookInterface } from "../../interfaces";

export default function BooksList({
  books,
  categorySelected,
  addToShelfButton,
}: {
  books: Array<BookInterface>;
  categorySelected: string;
  addToShelfButton: boolean;
}) {
  return (
    <Row lg={4}>
      {books
        .filter(
          (book: BookInterface) =>
            book.category === categorySelected || !categorySelected
        )
        .map((book: BookInterface, index: number) => (
          <Col className="mb-4" key={index}>
            <Book book={book} addToShelfButton={addToShelfButton} />
          </Col>
        ))}
    </Row>
  );
}
