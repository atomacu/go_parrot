import React, { useContext } from "react";
import { Card, ButtonGroup, Button } from "react-bootstrap";
import { ThemeContext } from "../../contexts";

export default function CategoryFilter({
  categorySelected,
  categories,
  toggleCategory,
}: {
  categorySelected: string;
  categories: Array<string>;
  toggleCategory: Function;
}) {
  const { themeVariant, toggleThemeVariant, themeTextVariant } = useContext(
    ThemeContext
  );
  return (
    <>
      <h4>Select category</h4>
      <Card className="rounded-0">
        <Card.Body className="p-0">
          <ButtonGroup vertical className="d-block rounded-0">
            {categories.map((category: string, index: number) => (
              <Button
                key={index}
                onClick={() => toggleCategory(category)}
                block
                className="rounded-0"
                variant={
                  category === categorySelected ? "warning" : themeVariant
                }
              >
                {category}
              </Button>
            ))}
          </ButtonGroup>
        </Card.Body>
      </Card>
    </>
  );
}
