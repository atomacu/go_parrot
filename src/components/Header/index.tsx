import React, { useContext } from "react";
import { ThemeContext } from "../../contexts";
import { Navbar, Nav, Container, Form } from "react-bootstrap";
export default function Header() {
  const { themeVariant, toggleThemeVariant, themeTextVariant } = useContext(
    ThemeContext
  );
  return (
    <Navbar
      bg={themeVariant}
      expand="lg"
      className={"text-" + themeTextVariant}
    >
      <Container>
        <Navbar.Brand href="/" className={"text-" + themeTextVariant}>
          Book library
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="/shelves" className={"text-" + themeTextVariant}>
              Shelves
            </Nav.Link>
            <Nav.Link
              href="/shelf-review"
              className={"text-" + themeTextVariant}
            >
              Shelf Review
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
        <span>
          Light
          <Form.Check
            onChange={toggleThemeVariant}
            type="switch"
            id="jhkjh"
            label=""
            inline
            className="pt-2 mx-3"
          />
          Dark
        </span>
      </Container>
    </Navbar>
  );
}
