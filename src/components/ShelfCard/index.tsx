import React, { useState, SyntheticEvent, useContext } from "react";
import BooksList from "../BooksList";
import Pointer from "../../styledComponents/Pointer";
import { Card, Modal, Row, Col, Button } from "react-bootstrap";
import { BookInterface, ShelfInterface } from "../../interfaces";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPencilAlt,
  faTimes,
  faStar,
} from "@fortawesome/free-solid-svg-icons";
import { ThemeContext } from "../../contexts";

export default function ShelfCard({
  books,
  shelf,
  setShelves,
}: {
  books: Array<BookInterface>;
  shelf: ShelfInterface;
  setShelves: Function;
}) {
  const { themeVariant, themeTextVariant } = useContext(ThemeContext);
  const [showShelf, setShowShelf] = useState(false);
  const shelfExample: ShelfInterface = {
    name: "",
    category: "",
    review: "",
    rating: -1,
    booksIds: [],
  };
  const [shelfModalTargetJson, setShelfModalTargetJson] = useState(
    shelfExample
  );
  const [shelfReviewEdit, setShelfReviewEdit] = useState(false);
  const handleCloseShelfModal = () => setShowShelf(false);
  const handleShowShelfModal = (shelf: any) => {
    setShelfModalTargetJson(shelf);
    setShowShelf(true);
  };

  const [hoverStar, setHoverStar] = useState(shelf.rating);

  const [review, setReview] = useState("");
  const handleReview = (e: SyntheticEvent) => {
    //@ts-ignore
    setReview(e.currentTarget.value);
  };

  const handleReviewSubmit = (e: SyntheticEvent) => {
    e.preventDefault();
    let localShelf: ShelfInterface = { ...shelf };
    let localShelves: Array<ShelfInterface> = JSON.parse(
      localStorage.getItem("shelves")
    );

    localShelf.review = review;
    localShelves = localShelves.reduce((acc, item) => {
      console.log(item.name !== localShelf.name);
      if (item.name !== localShelf.name) return [...acc, item];
      return [...acc, localShelf];
    }, []);

    localStorage.setItem("shelves", JSON.stringify(localShelves));
    setShelves(localShelves);
    handleCloseShelfModal();
    setShelfReviewEdit(false);
  };

  const handleSetRatingSubmit = (index: number) => {
    let localShelf: ShelfInterface = { ...shelf };
    let localShelves: Array<ShelfInterface> = JSON.parse(
      localStorage.getItem("shelves")
    );

    localShelf.rating = index;
    localShelves = localShelves.reduce((acc, item) => {
      console.log(item.name !== localShelf.name);
      if (item.name !== localShelf.name) return [...acc, item];
      return [...acc, localShelf];
    }, []);

    localStorage.setItem("shelves", JSON.stringify(localShelves));
    setShelves(localShelves);
    handleCloseShelfModal();
    setShelfReviewEdit(false);
  };

  const handleReviewDelete = () => {
    let localShelf: ShelfInterface = { ...shelf };
    let localShelves: Array<ShelfInterface> = JSON.parse(
      localStorage.getItem("shelves")
    );

    localShelf.review = "";
    localShelves = localShelves.reduce((acc, item) => {
      console.log(item.name !== localShelf.name);
      if (item.name !== localShelf.name) return [...acc, item];
      return [...acc, localShelf];
    }, []);

    localStorage.setItem("shelves", JSON.stringify(localShelves));
    setShelves(localShelves);
    handleCloseShelfModal();
    setShelfReviewEdit(false);
  };

  const handleShelfDelete = () => {
    let localShelf: ShelfInterface = { ...shelf };
    let localShelves: Array<ShelfInterface> = JSON.parse(
      localStorage.getItem("shelves")
    );
    localShelves = localShelves.reduce((acc, item) => {
      console.log(item.name !== localShelf.name);
      if (item.name !== localShelf.name) return [...acc, item];
      return [...acc];
    }, []);

    localStorage.setItem("shelves", JSON.stringify(localShelves));
    setShelves(localShelves);
    handleCloseShelfModal();
    setShelfReviewEdit(false);
  };

  return (
    <>
      <Pointer onClick={() => handleShowShelfModal(shelf)}>
        <Card className="rounded-0">
          <Card.Body
            className={"bg-" + themeVariant + " text-" + themeTextVariant}
          >
            <h3 className="mb-0 text-center">{shelf.name}</h3>
            <Card.Text className="text-center mb-0">
              <span className="font-weight-bold">Category: </span>
              {shelf.category}
            </Card.Text>
          </Card.Body>
        </Card>
      </Pointer>

      <Modal size="lg" show={showShelf} onHide={handleCloseShelfModal}>
        <Modal.Body>
          <h3 className="text-center">{shelfModalTargetJson.name}</h3>
          <p className="text-center mb-0">
            <span className="font-weight-bold">Category: </span>
            {shelfModalTargetJson.category}
          </p>

          {!shelf.review || shelfReviewEdit ? (
            <form onSubmit={handleReviewSubmit}>
              <Row>
                <Col>
                  <textarea
                    onChange={handleReview}
                    className="form-control rounded-0"
                    placeholder="Shelf review"
                    defaultValue={shelf.review}
                  ></textarea>
                </Col>
              </Row>
              <Row className="mt-3 mb-4">
                <Col>
                  <Button type="submit" block className="rounded-0">
                    Add shelf review
                  </Button>
                </Col>
              </Row>
            </form>
          ) : (
            <Row>
              <Col>
                <p>
                  <span className="font-weight-bold">Review: </span>
                  {shelf.review}
                </p>
              </Col>
              <Col xs={1}>
                <Pointer onClick={() => setShelfReviewEdit(true)}>
                  <FontAwesomeIcon
                    className="text-success h3"
                    icon={faPencilAlt}
                  />
                </Pointer>
              </Col>
              <Col xs={1}>
                <Pointer onClick={handleReviewDelete}>
                  <FontAwesomeIcon className="text-danger h3" icon={faTimes} />
                </Pointer>
              </Col>
            </Row>
          )}

          <Row>
            <Col>
              {[0, 1, 2, 3, 4].map((index: number) => (
                <FontAwesomeIcon
                  onMouseOver={() => shelf.rating === -1 && setHoverStar(index)}
                  onMouseLeave={() => shelf.rating === -1 && setHoverStar(-1)}
                  onClick={() =>
                    shelf.rating === -1 && handleSetRatingSubmit(index)
                  }
                  icon={faStar}
                  className={
                    hoverStar >= index
                      ? "text-warning h3 pointer"
                      : "h3 pointer"
                  }
                />
              ))}
            </Col>
          </Row>
          <hr />
          <BooksList
            books={books.filter((item) =>
              shelfModalTargetJson.booksIds.includes(item.id)
            )}
            categorySelected={""}
            addToShelfButton={false}
          />
          <Button
            onClick={handleShelfDelete}
            variant="danger"
            block
            className="rounded-0 font-weight-bold mt-2"
          >
            Delete shelf
          </Button>
        </Modal.Body>
      </Modal>
    </>
  );
}
