import React from "react";
import ShelfCard from "../ShelfCard";
import { Row, Col } from "react-bootstrap";
import { ShelfInterface, BookInterface } from "../../interfaces";

export default function ShelvesList({
  shelves,
  books,
  setShelves,
}: {
  shelves: Array<ShelfInterface>;
  books: Array<BookInterface>;
  setShelves: Function;
}) {
  return (
    <Row xs={4} className="mt-3">
      {shelves &&
        shelves.map((shelf: ShelfInterface, index: number) => (
          <Col className="mb-4" key={index}>
            <ShelfCard books={books} shelf={shelf} setShelves={setShelves} />
          </Col>
        ))}
    </Row>
  );
}
