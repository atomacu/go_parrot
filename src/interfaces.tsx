export interface BookInterface {
  id: string;
  title: string;
  author: string;
  description: string;
  cover_img: string;
  category: string;
  pages: number;
}

export interface ShelfInterface {
  name: string;
  category: string;
  review: string;
  rating: number;
  booksIds: Array<string>;
}
