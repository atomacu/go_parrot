import React, { useState } from "react";
import Shelves from "../views/Shelves";
import Wellcome from "../views/Wellcome";
import { ThemeContext } from "../contexts";
import ShelfReview from "../views/ShelfReview";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  const [themeVariant, setThemeVariant] = useState("light");
  const [themeTextVariant, setThemeTextVariant] = useState("dark");

  const toggleThemeVariant = () => {
    setThemeVariant(themeVariant === "light" ? "dark" : "light");
    setThemeTextVariant(themeTextVariant === "dark" ? "light" : "dark");
  };
  return (
    <Router>
      <Switch>
        <ThemeContext.Provider
          value={{ themeVariant, toggleThemeVariant, themeTextVariant }}
        >
          <Route key="wellcome" exact path="/" component={Wellcome} />
          <Route
            key="shelf-review"
            exact
            path="/shelf-review"
            component={ShelfReview}
          />
          <Route key="shelves" exact path="/shelves" component={Shelves} />
        </ThemeContext.Provider>
      </Switch>
    </Router>
  );
}

export default App;
