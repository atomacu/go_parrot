import books from "../../books";
import React, { useState } from "react";
import { Container } from "react-bootstrap";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import { ShelfInterface } from "../../interfaces";
import ShelvesList from "../../components/ShelvesList";

export default function ShelfReview() {
  const [shelves, setShelves] = useState(
    JSON.parse(localStorage.getItem("shelves"))
  );
  return (
    <>
      <Header />
      <Container>
        <ShelvesList
          books={books}
          shelves={
            shelves &&
            shelves.filter((shelf: ShelfInterface) => shelf.review !== "")
          }
          setShelves={setShelves}
        />
      </Container>

      <Footer />
    </>
  );
}
