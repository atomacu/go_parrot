import React, { useState } from "react";
import books from "../../books";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import { BookInterface } from "../../interfaces";
import { Container, Row, Col } from "react-bootstrap";
import ShelvesList from "../../components/ShelvesList";
import AddShelfModal from "../../components/AddShelfModal";

export default function Shelves() {
  const [shelves, setShelves] = useState(
    JSON.parse(localStorage.getItem("shelves"))
  );
  const categories = [
    //@ts-ignore
    ...new Set(books.map((book: BookInterface) => book.category)),
  ];

  return (
    <>
      <Header />
      <Container>
        <Row className="justify-content-end mt-3">
          <Col md={3}>
            <AddShelfModal categories={categories} setShelves={setShelves} />
          </Col>
        </Row>
        <ShelvesList books={books} shelves={shelves} setShelves={setShelves} />
      </Container>

      <Footer />
    </>
  );
}
