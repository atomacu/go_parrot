import books from "../../books";
import React, { useState } from "react";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import { BookInterface } from "../../interfaces";
import BooksList from "../../components/BooksList";
import { Row, Col, Container } from "react-bootstrap";
import CategoryFilter from "../../components/CategoryFilter";

export default function Wellcome() {
  const [categorySelected, setCategorySelected] = useState("");

  const toggleCategory = (category: string) => {
    setCategorySelected(categorySelected !== category ? category : "");
  };

  const categories = [
    //@ts-ignore
    ...new Set(books.map((book: BookInterface) => book.category)),
  ];
  return (
    <>
      <Header />
      <Container className="justify-content-center">
        <Row className="mt-3">
          <Col md={3}>
            <CategoryFilter
              toggleCategory={toggleCategory}
              categorySelected={categorySelected}
              categories={categories}
            />
          </Col>
          <Col md={9}>
            <BooksList
              books={books}
              addToShelfButton={true}
              categorySelected={categorySelected}
            />
          </Col>
        </Row>
      </Container>
      <Footer />
    </>
  );
}
